import React from 'react'

import './App.css'

export class CreatePost extends React.Component {
    constructor(props) {
        super(props)

        this.state = { postText: '' }

        this.updatePostText = this.updatePostText.bind(this)
        this.submitPostText = this.submitPostText.bind(this)
    }

    updatePostText(event) {
        this.setState({ postText: event.target.value })
    }

    submitPostText() {
        if (this.state.postText.length > 0) {
            this.props.post(this.state.postText)
 
            this.setState({ postText: '' })
        }
    }

    render() {
        return (
            <div className="mb-2 fixedMenuFix">
                <input
                    id="postField"
                    value={this.state.postText}
                    onChange={this.updatePostText}
                    type="text"
                    className="form-control"
                    placeholder="What's up?"
                />
                <button onClick={this.submitPostText} className="btn-post"><span>Tweet</span></button>
            </div>
        )
    }
}



