import React from 'react'

import { Post } from './Post'
import './App.css'


export class UserFeed extends React.Component {
    render() {
        return (
            <>
                {this.props.postData.map(post =>
                    <Post postData={post} key={post.id} />
                )}
            </>
        )
    }
}
