import React from 'react'

import './App.css'


export class PostContent extends React.Component {
    render() {
        const imageSrc = "https://picsum.photos/50?image" + String(this.props.postData.userId)

        return (
            <div>
                <div className="col-12 d-flex align-items-center">
                    <img className={"profileThumb"} src={imageSrc} />
                    <h3 className="font-weight-bold ml-2 mb-1 d-inline-block">{this.props.postData.name}</h3>
                    <a href="#" className="ml-2 text-muted">@{this.props.postData.username}</a>
                </div>
                <div className="col-12">
                    <p className="mb-0">{this.props.postData.body}</p>
                </div>
            </div>
        )
    }
}

