import React from 'react'

import { CreatePost } from './CreatePost'
import { Feed } from './Feed'
import { UserBar } from './UserBar'
import { UserFeed } from './UserFeed'
import './App.css'

export class Home extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            allPosts: [{
                title: 'LOADING',
                body: 'LOADING',
                name: 'LOADING',
                username: '@LOADING'
            }],
            userPosts: []
        }

        this.sortPosts = this.sortPosts.bind(this)
        this.matchAccounts = this.matchAccounts.bind(this)
        this.userPost = this.userPost.bind(this)

        this.loaded = false
        this.postDataWithoutUserInfo = []
        this.newUserPosts = []
    }

    componentDidMount() {
        let localPosts = JSON.parse(localStorage.getItem('userPosts'))

        if (localPosts) {
            this.setState({ userPosts: localPosts })
        }

        if (this.loaded === false) {
            fetch('https://jsonplaceholder.typicode.com/posts')
                .then(response => response.json())
                .then(json => this.sortPosts(json))
                .then(() => fetch('https://jsonplaceholder.typicode.com/users'))
                .then(response => response.json())
                .then(json => this.matchAccounts(json))
                .then(json => this.setState({ allPosts: json }))
                .catch(error => console.log(error))
        }

        this.loaded = true
    }

    sortPosts(array) {
        let currentIndex = array.length, temporaryValue, randomIndex

        while (0 !== currentIndex) {
            randomIndex = Math.floor(Math.random() * currentIndex)
            currentIndex--

            temporaryValue = array[currentIndex]
            array[currentIndex] = array[randomIndex]
            array[randomIndex] = temporaryValue
        }

        this.postDataWithoutUserInfo = array

        return array
    }

    matchAccounts(userData) {
        let completeFeedData = []

        for (let i = 0; i < this.postDataWithoutUserInfo.length; i++) {
            for (let j = 0; j < userData.length; j++) {
                if (this.postDataWithoutUserInfo[i].userId === userData[j].id) {
                    completeFeedData.push({ ...this.postDataWithoutUserInfo[i], ...userData[j] })
                }
            }
        }

        return completeFeedData
    }

    userPost(post) {
        let totalLength = this.state.allPosts.length + this.state.userPosts.length
        let newPostData = {
            userId: 11,
            id: totalLength + 2,
            name: 'You',
            username: 'bitter_user',
            body: post,
            userPost: true
        }

        this.newUserPosts.unshift(newPostData)

        this.setState({
            userPosts: this.newUserPosts
        })
    }
    render() {
        return (
            <div>
                <UserBar />
                <div className="container">
                    <CreatePost post={this.userPost} />
                </div>
                <div className="container">
                    {this.state.userPosts.length > 0 && <UserFeed postData={this.state.userPosts} />}
                    <Feed postData={this.state.allPosts} />
                </div>
            </div>
        )
    }

    _updateLocalStorage = () => {
        let userPosts = JSON.stringify(this.state.userPosts);
        localStorage.setItem('posts', userPosts)
    }
}
