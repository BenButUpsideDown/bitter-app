import React from 'react'

import './App.css'

export class PostActions extends React.Component {
    constructor(props) {
        super(props)

        this.state = { liked: false }

        this.toggleLike = this.toggleLike.bind(this)
    }

    toggleLike() {
        this.setState({ liked: !this.state.liked })
    }

    render() {
        return (
            <div className="col-sm-12 mt-1">
                <button
                    onClick={this.toggleLike}
                    className={this.state.liked ? 'btn-red' : 'btn-green'}
                >
                    {this.state.liked ? 'Hate me' : 'Love me'}
                </button>
                <button onClick={this.props.replyWindowOpen} className="btn-reply">Reply</button>
            </div>
        )
    }
}
