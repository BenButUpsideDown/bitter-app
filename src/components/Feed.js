import React from 'react'

import { Post } from './Post'
import './App.css'


export class Feed extends React.Component {
    render() {
        return (
            <div>
                {this.props.postData.map((post, key) =>
                    <Post postData={post} key={post.id} />
                )}
            </div>
        )
    }
}


